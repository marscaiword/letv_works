﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(LETV_WORKS.Startup))]
namespace LETV_WORKS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
